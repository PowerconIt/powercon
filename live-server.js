var chokidar = require("chokidar");
const WebSocket = require('ws');
const detect = require('detect-port');

var port = 8872;
var wss;

detect(port, (err, _port) => {

    if (err) {console.log(err);}
    
    // available
    if (port == _port) {

        wss = new WebSocket.Server({port: port});
        watchFiles();
    
    // occupied
    } else {
      //console.log(`port: ${port} was occupied, try port: ${_port}`);
    }
});
  

function watchFiles(){

    var update = false;
    var files=[]; // files that changed
    
    chokidar.watch(['public','resources/views']).on('all', (event, path) => {
    
        files.push({event, path});
        
        // can update now
        if(update == false){
            
            setTimeout(()=>{
    
                var fileString = JSON.stringify(files);
                
    
                if(fileString.indexOf(`.js"`)>=0 || fileString.indexOf(`.js'`) >=0 ){
                    webUpdateBrowser('js');
                }else if(fileString.indexOf(`.css"`)>=0 || fileString.indexOf(`.css'`) >=0){
                    webUpdateBrowser('css');
                }else if(fileString.indexOf(`.blade.php"`)>=0 || fileString.indexOf(`.blade.php'`) >=0){
                    webUpdateBrowser('view');
                }
    
                update = false;//give posibility for update
                files=[];
            },100);
    
            update = true;//update no more
        }    
    
    });
    
}


function webUpdateBrowser(type){
    console.log(type);
    
    //loop throught each connection
    wss.clients.forEach(function(ws){
        // connection is ready
        if(ws.readyState==1){
            ws.send(type); // send signal
        }
        
    });	
}