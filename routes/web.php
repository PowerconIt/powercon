<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
    count_visitors($request);
    return view('index');
});


Route::get('/perfil', function (Request $request) {
    count_visitors($request);
    return view('profile');
});

Route::post('/sendMail','emailContorller@sendEmail');



Route::get('/galeria', function (Request $request) {
    count_visitors($request);
    return view('galary');
});


Route::get('/admin', function (Request $request) {
    
    count_visitors($request);
    
    $visitors = DB::table('visitors as e')
        ->orderBy('e.id', 'DESC')
        ->limit(100)
        ->get();

    $uniqueVisitors = DB::table('visitors')->groupBy('ip')->get();

    $today = DB::table('visitors')->where('date', 'like', date("Y/m/d"))->get();

    return view('admin', 
        [
            'visitors' => $visitors,
            'date' => date("Y/m/d"),
            'totalVisitors' => $visitors[0]->id,
            'today' => $today->count(),
            'uniqueVisitors' => $uniqueVisitors->count()
        ]
    );
});



function count_visitors ($request) {
    if( $request->ip() == '127.0.0.1' ) {
        $IP = '41.72.16.78';
    } else { 
        $IP = $request->ip();
    }

    $ipdat = @json_decode(file_get_contents( 
        "http://www.geoplugin.net/json.gp?ip=" . $IP)); 

    $data = array(
        'browser' => $request->header('User-Agent'),
        'date' => date("Y/m/d"),
        'time' => date("h:i:sa"),
        'ip' => $IP,
        'country' => $ipdat->geoplugin_countryName,
        'Latitude' => $ipdat->geoplugin_latitude,
        'Longitude' => $ipdat->geoplugin_longitude,
        'Continent' =>  $ipdat->geoplugin_continentName
    );

    DB::table('visitors')->insert($data);

}