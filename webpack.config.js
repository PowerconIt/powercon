const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

var files = {
    //  input                        public/js/out                       
    "./resources/js/index/index.js":"index/index.js",
	"./resources/js/galary/galaryShowMore.js":"galary/galaryShowMore.js", 
	"./resources/js/admin/auth.js":"admin/auth.js" 
}
   
var fileToUse = {};


for(var file in files){
  fileToUse[files[file]] = file
}

module.exports = {
	entry: fileToUse,
	output: {
		filename: '[name]',
		path: path.resolve(__dirname, 'public/js')
	},
	module:{
		rules:[
			{
				test:/.(js|jsx)$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			}
		]
	},
	resolve: { alias: { vue: 'vue/dist/vue.esm.js' } },
  	plugins: [
		new CleanWebpackPlugin(),
		new VueLoaderPlugin()
	],

}

if(process.env.NODE_ENV=='development'){
	console.log('dev')
	module.exports.devtool= '#eval-source-map';
	module.exports.optimization={
		minimize: false
	}

}else{
	console.log('production')
	module.exports.optimization={
		minimize: true
	}  
} 