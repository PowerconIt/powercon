const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var config = {
    module: {
        rules: [

        ]
    },
    plugins: [
        
    ],

}

if(process.env.NODE_ENV=='development'){
    config.devtool= '#eval-source-map';
    config.optimization={
		minimize: false
	}

}else{
    config.optimization={
		minimize: true
	}  
}    

mix.webpackConfig(config).sass('resources/sass/galary.sass', 'public/css')
.sass('resources/sass/home.sass', 'public/css')
.sass('resources/sass/glider.sass', 'public/css')
.sass('resources/sass/auth/auth.sass', 'public/css')