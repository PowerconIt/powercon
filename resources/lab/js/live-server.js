(function() {


    function refreshCSS() {
        var sheets = [].slice.call(document.querySelectorAll("head link"));
        var head = document.getElementsByTagName("head")[0];
        for (var i = 0; i < sheets.length; ++i) {
            var elem = sheets[i];
            head.removeChild(elem);
            var rel = elem.rel;
            if (elem.href && typeof rel != "string" || rel.length == 0 || rel.toLowerCase() == "stylesheet") {
                var url = elem.href.replace(/(&|\?)_cacheOverride=\d+/, '');
                elem.href = url + (url.indexOf('?') >= 0 ? '&' : '?') + '_cacheOverride=' + (new Date().valueOf());
            }
            head.appendChild(elem);
        }
    }

    function connectWebsocket(){

        var ip = window.location.hostname;
 

        var protocol = window.location.protocol === 'http:' ? 'ws://' : 'wss://';
        var address = protocol + window.location.hostname +":1727" + '/ws';
        var socket = new WebSocket(address);
    
        socket.onmessage = function (e){
            if (msg.data == 'reload') window.location.reload();
            else if (msg.data == 'refreshcss') refreshCSS();
        };
    
        socket.onclose = function (params) {
            connectWebsocket();
        };
    
        socket.onopen= function (params) {
            
        };
    
    }

    /**
     * 
     * @param {String} address Hostname
     * @return boolean 
     */

    function isIP( address ){ 
        r = RegExp('^http[s]?:\/\/((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])');
        return r.test( address )
    }

    // if WebSocket is avaivle and hot is Ip
    if ('WebSocket' in window  && !isIP()) {
        connectWebsocket()
    }

})();