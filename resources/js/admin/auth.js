
import Vue from "vue";
import Cookies from 'universal-cookie';


require("../bootstrap.js");


const cookies = new Cookies();
 
window.cookies = cookies;

document.addEventListener('DOMContentLoaded', (event) => {

    new Vue({
        el:".mainApp",
        data(){
            return {
                error:false,
                loading:false,
                user:"",
                password:"",
                auth: true,
                admin: false,
            }
        },
        methods: {
            login(a){   

                setTimeout(()=>{
                    // hide loading box
                    this.loading = false;
                    this.error = false;
                    
                    if(this.user == "powercon" && this.password == 'powercon1726'){ 
                        this.auth = false;
                        cookies.set('alive', 'alive', { maxAge: 900 });
                    }else{
                        // show run error message animation
                        this.error = true;
                        cookies.remove('alive')
                    }
                },100);

            },
            logOut() {
                this.user = "";
                this.password = "";

                setTimeout(()=>{
                    this.auth = true;
                },100);

                
            }
        },
        mounted () {

            console.log('!!!')
            // login restored session on cookies
            if (cookies.get('alive') == 'alive') {
                this.auth = false;
            }
        }
    });

});