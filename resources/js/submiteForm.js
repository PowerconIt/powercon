export default ()=>{

}

function p(x){return document.querySelector(x);}   

// form submite
function formValidation(email, Subject, Message){
    //email
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
        // email not valid
        p("#form_email").className="error"
        //console.log('invalide emila');
        return false;
    }else{
        p("#form_email").className="";
    }

    if(Subject==""){
        // Message not valid 
        p('#form_subject').className="error"
        //console.log('invalid suvvvv');
        return false;
    }else{
        p('#form_subject').className="";
        
    }

    if(Message==""){
        // Message not valid 
        p('#form_textarea').className="error"
        //console.log('invalid message');
        return false;
    }else{
        p('#form_textarea').className="";
    }


    return true;
}

document.addEventListener('DOMContentLoaded', (event) => {

    p("#submiteForm").addEventListener('click', ()=>{
    
        var email = p("#form_email").value;
        var Subject = p('#form_subject').value;
        var Message = p('#form_textarea').value;
    
    
        
        if(formValidation(email, Subject, Message)){
    
            var params = {
                email: email,
                Subject: Subject,
                Message: Message,
            }
            
            axios.post('/sendMail', params).then((e)=>{

                p("#form_email").value = "";
                p('#form_subject').value= "";
                p('#form_textarea').value= "";

                p('.contacte-nos').className="contacte-nos contacte-nos-disabled";
            }).catch(e => {
                console.log(e)
            });;
    
        }else{
            console.log('invalid')
        }
        
    });
});
