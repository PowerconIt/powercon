require("../bootstrap.js");
import glider from 'glider-js';
import "../submiteForm";


window.Glider = glider;
document.addEventListener('DOMContentLoaded', (event) => {
   
    (function (){
        // nav
        function p(x){
            return document.querySelector(x);
        }   
        
        p(".openContact").addEventListener('click',()=>{
            p('.contacte-nos').className="contacte-nos";
        });

        p(".close_contact").addEventListener('click',()=>{
            p('.contacte-nos').className="contacte-nos contacte-nos-disabled";
        });

        
        // header background
        var background = document.querySelector('.header_main .background');
        


        function setBackgroundHeight(){
        
            if((window.innerHeight - 80)>=439){
                background.style.height = window.innerHeight - 80+"px";
            }
        }

        if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            console.log('not mobile')
            window.onresize=function(){
                setBackgroundHeight();
            }
            setBackgroundHeight();
        }
        

        // Header float
        var nav =  document.querySelector('.nav');
        var main_content_info = document.querySelector('.main_content .info');

        nav.style.padding='0px 4%';
        

        new Glider(document.querySelector('.glider'), {
            slidesToShow: 3,
            slidesToScroll: 1,
            draggable: true,
            dots: '#dots',
            arrows: {
                prev: '.glider-prev',
                next: '.glider-next'
            }
        });


    })();

});