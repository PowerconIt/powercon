<div class="contacte-nos contacte-nos-disabled">
    
    <div class="conteiner">
        <div class="close_contact"><i class="fas fa-times-circle"></i></div>
        <div class="side_bar">
            <p class="title">Contacta-nos</p>
            <p><i class="fas fa-map-marker-alt"></i> Bairro Chinguar III Travessa B/22 S/N Benfica</p>
            <p><i class="far fa-envelope"></i> geral@powerconlda.com</p>
            <p><i class="fas fa-mobile-alt"></i> 933 527 548</p>
        </div>

        <div class="form">
            <p class="title">Entrar em contato</p>
            <p class="sinta">Sinta à vontade </p>
            <p><input id="form_email" name="email" type="text" placeholder="Email"></p>
            <p><input id="form_subject" name="subject" type="text" placeholder="Assunto"></p>
            <p><textarea id="form_textarea" placeholder="Mensagem"></textarea></p>

            <div class="button" id="submiteForm">Enviar</div>
        </div>

    </div>


</div>

