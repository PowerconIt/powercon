<script type="text/javascript">
	(function() {

		// refresh css
		function refreshCSS() {
			var sheets = [].slice.call(document.querySelectorAll("head link"));
			var head = document.getElementsByTagName("head")[0];
			for (var i = 0; i < sheets.length; ++i) {
				var elem = sheets[i];
				head.removeChild(elem);
				var rel = elem.rel;
				if (elem.href && typeof rel != "string" || rel.length == 0 || rel.toLowerCase() == "stylesheet") {
					var url = elem.href.replace(/(&|\?)_cacheOverride=\d+/, '');
					elem.href = url + (url.indexOf('?') >= 0 ? '&' : '?') + '_cacheOverride=' + (new Date().valueOf());
				}
				head.appendChild(elem);
			}
		}

		/**
		 * @description web socket 
		*/
		function connectWebsocket(){

			var protocol = window.location.protocol === 'http:' ? 'ws://' : 'wss://';
			var address = protocol + window.location.hostname +":8872" + '/ws';
			var socket = new WebSocket(address);

			// on file change
			socket.onmessage = function (msg){
				if (msg.data == 'js') window.location.reload(); 
				else if (msg.data == 'css') refreshCSS(); 
				else if (msg.data == 'view') window.location.reload(); 
			};

			// on faild to connect
			socket.onclose = function (params) {
				// reconnect
				setTimeout(()=> {
					connectWebsocket();
				},1000);
			};

		}


		/**
		 * 
		 * @param {String} address Hostname
		 * @return boolean 
		 */

		var location = function () {
        var protocol = window.location.protocol;
        var host = window.location.host;
        return protocol+"//"+host+"/";
    }
		//is hot not ip
		function isIP(url){
  
			var regex = /^(http|https):\/\/[0-9_.]{0,3}[0-9_.]{0,3}[0-9_.]{0,3}/
			var match = url.match(regex);

			if (match === null) {
					return false;
			} else {
					return true;
			}

    };

		// if WebSocket is avaivle and hot is Ip
		if ('WebSocket' in window  && !isIP(location())) {
			connectWebsocket();
		}

	})();
		
	// ]]>
</script>