@extends('base')

@section('title','Powercon')

@section('header')
    <style>
        body{margin:0;font-family:sans-serif;height:100%;width:100%;position:absolute;top:0;left:0;z-index:2;min-width:950px;font-family:Krub,sans-serif}body header.header_main{width:100%;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}body header.header_main .background{box-shadow:inset 0 -39px 97px #dedede;overflow:hidden}body header.header_main .background .conteiner{height:100%;width:100%;display:flex}body header.header_main .background .conteiner .gerador{height:100%;display:flex;width:40%;padding:67px 2% 4% 4%}body header.header_main .background .conteiner .gerador .img_conteiner{margin:0 auto;width:100%}body header.header_main .background .conteiner .gerador .img_conteiner img{width:100%}body header.header_main .background .conteiner .gerador .img_conteiner img:first-child{margin-bottom:-6px}body header.header_main .background .conteiner .info{font-size:15pt;color:#333;max-width:762px;padding:4% 4% 4% 2%;width:50%;text-align:justify;font-family:Krub,sans-serif;font-weight:500}body header.header_main .background .logo{margin-top:-150px;padding:0 4% 50px;font-family:Krub,sans-serif}body header.header_main .background .logo h1,body header.header_main .background .logo h2{color:#5e5e5e;margin:0}body header.header_main .background .logo h1{font-size:35px;font-weight:400}body header.header_main .background .logo h2{font-size:18px;font-weight:500}body header.header_main div.nav{display:flex;background:#fff;border-bottom:1px solid #d6d6d6;transition:1s;z-index:10000}body header.header_main div.nav .logo{width:80px}body header.header_main div.nav .logo img{width:100%}body header.header_main div.nav nav{z-index:100000}body header.header_main div.nav nav ul{margin-top:26px}body header.header_main div.nav nav ul li{display:inline;padding-left:30px;font-weight:600;font-size:13pt;cursor:pointer}body header.header_main div.nav nav ul li a{color:#000;text-decoration:none}body header.header_main .fixed{position:fixed;top:0;width:100%;box-shadow:0 10px 10px hsla(0,0%,79.6%,.47);border-bottom:1px solid #b9b9b9}body main.main_content{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;z-index:100;font-family:Krub,sans-serif}body main.main_content .activities,body main.main_content .contact,body main.main_content .galary,body main.main_content .info{border-bottom:1px solid #d6d6d6;display:flex;justify-content:space-between;padding-top:40px;padding-bottom:60px}body main.main_content .activities .left,body main.main_content .contact .left,body main.main_content .galary .left,body main.main_content .info .left{padding:0 0 0 5%}body main.main_content .activities .left .title,body main.main_content .contact .left .title,body main.main_content .galary .left .title,body main.main_content .info .left .title{max-width:300px;font-weight:600}body main.main_content .activities .right,body main.main_content .contact .right,body main.main_content .galary .right,body main.main_content .info .right{padding:0 5%;text-align:justify;width:60%;font-size:13pt;font-weight:400}body main.main_content .activities .right ul{padding:0;margin:0 0 0 21px;list-style:decimal;font-size:13pt;font-weight:500}body main.main_content .activities .right ul li{margin-bottom:5px}body main.main_content .activities .right ul ul{margin-bottom:15px;font-weight:400;list-style:disc}body main.main_content .activities .right ul ul ul{margin-bottom:5px;list-style:lower-alpha}body main.main_content .galary{background-color:#2d2d2d;box-shadow:inset 0 -52px 67px #1d1d1d;display:block;z-index:100;padding-bottom:40px}body main.main_content .galary p.showMore{color:#fff;text-align:center;margin-bottom:0}body main.main_content .galary p.arrow{margin:0;cursor:pointer;color:#fff;text-align:center}body main.main_content .galary p.title{text-align:center;margin:0 0 10px;font-weight:600;color:#fff}body main.main_content .galary .slide{display:flex;justify-content:center;width:100%}body main.main_content .galary .slide .container{width:95%;position:relative;color:#fff}body main.main_content .galary .slide .container #dots .active{background-color:#fff}body footer.footer_main{display:flex;padding-top:40px;padding-bottom:40px;justify-content:center}body footer.footer_main .left{display:flex;flex-wrap:wrap}body footer.footer_main .left p{margin:0;width:100%;padding-top:23px}body footer.footer_main .left .powercon{font-size:28pt;text-align:center}body footer.footer_main .left .con1{font-size:17pt!important;text-align:center}body footer.footer_main .left .con2,body footer.footer_main .left .con3,body footer.footer_main .left .con4,body footer.footer_main .left .con5,body footer.footer_main .left .con6,body footer.footer_main .left .con7{font-size:11pt;text-align:center;color:#49688d}
    </style>
    
    <link href="https://fonts.googleapis.com/css2?family=Krub:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;1,200;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">    
    <link rel="stylesheet" href="/css/glider.css">
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.7/css/all.css">        
    <script src="/js/index/index.js"></script>
    <link rel="stylesheet" href="/css/home.css">

@endsection

@section('content')

@include("include.contacte-nos")
<style>
    .main_content{
        justify-content: center;
        display: flex;
        flex-wrap: wrap;
    }   
</style>

<header class="header_main">
    
    <div class="background">
        <div class="conteiner">
            <div class="gerador">
                <div class="img_conteiner">
                    <img src="https://powerconlda.com/img/gerador.png">
                    <img src="https://powerconlda.com/img/gerador-shadow.png">
                </div>
                
            </div>
            <div class="info">
                <p>Somos, uma empresa formada segundo as regras do direito angolano, registada no Ministério das Obras Públicas sob o Nº 0196/01 e portadora do cartão de contribuinte Nº 0.159.361/00-1, com data de 20 de Maio de 2002, cuja escritura de constituição se encontra publicada na III Série do Diário da República Nº 55, de 12 de Julho de 2002. 
                </p>
                <p>Fornecimento de materiais para construção civil                            </p>
            </div>
        </div>       
        
        <div class="logo">
            <h1>POWERCON, LDA</h1>
            <h2>Power Construction Limitada</h2>
        </div>
                    
    </div>
    <div class="nav" >
        <div class="logo">
            <img src="https://powerconlda.com/img/logo.png">
        </div>
        <nav>
            <ul>
                <li><a href="/">Inicío</a></li>
                <li> <a href="/#actividades_hash">Áreas e actividades do nosso objecto social</a>  </li>
                <li><a href="/galeria">Galeria</a></li>
                <li class="openContact">Contacte-nos</li>
                <li><a href="https://www.facebook.com/Powercon-Construction-Limitada-111545377422553/">Facebook</a></li>
                <li><a href="/perfil">Perfil</a></li>
            </ul>
        </nav>
    </div>



</header>
<main class="main_content">
    <section class="info">
        <div class="left" id="actividades_hash">
            <div id="cover">
                <p class="title">QUEM SOMOS</p>
            </div>
            
        </div>
        <div class="right">A Powercon Construction, Limitada é uma empresa de direito angolano, registada no
            Ministério das Obras Públicas sob o Nº 0196/2001, titular do Cartão de contribuinte
            Nº 5403083090, com escritura de constituição publicada na III Série do Diário da
            República Nº 55 de 12 de Junho de 2002, com a Sede em Luanda, no Alameda
            Manuel Van-Duném Nº 303 / 305 e Escritório Bairro Chinguar III Benfica.
            Há mais de 20 anos que os nossos produtos e serviços contribuem para a melhoria de
            muitas vidas em Angola e para a melhoria dos padrões de vida. Os nossos clientes
            empregam os nossos produtos fiáveis para construir infraestruturas básicas, orgulhamonos de ajudar a fornecer às pessoas o acesso a redes de água, eletricidade e estradas,
            hospitais e muito mais.
        </div>
    </section>
    <img src="https://powerconlda.com/img/intro.jpg">
    <section class="activities">
        <div class="left">
            <p class="title">MISSÃO</p>
        </div>
        <div class="right">
        Vender e fornecer manutenção a geradores de energia da mais alta qualidade, levando
energia aos nossos clientes e comunidades locais permitindo-lhes prosperar trazendolhes os blocos de construção mais básicos mas vitais das suas próprias vidas modernas. 
        </div>
    </section>

    <section class="activities">
        <div class="left">
            <p class="title">VISÃO</p>
        </div>
        <div class="right">
        As nossas ambições futuras são extrair energia de qualquer fonte e expandir a
sustentabilidade. Alcançaremos estes objetivos consolidando continuamente a nossa
posição no mercado angolano e não só aguçando a agilidade que nos caracteriza.
        </div> 
        </div>
    </section>
    <div class="right">
    </section>
    <section class="activities">
        <div class="left">
            <p class="title">VALORES</p>
        </div>
        <div class="right">
        Foco do cliente; Inovação; Lealdade; Integridade e Ética;
        </div>
    </section>

    <section class="activities">
        <div class="left">
            <p class="title">
                <img style="width: 222px" src="https://powerconlda.com/icons/crhis.png">
            </p>
        </div>
        <div class="right">
        “We need to empower our
        communities to see their vital
        change, we will feed them with
        energy, and they will give us
        tomorrow’s brighter future”
        </div>
    </section>

    
    <section class="activities">
        <div class="left">
            <p class="title">O QUE FAZEMOS</p>
        </div>
        <div class="right">
         <p>A POWERCON, Limitada, tem já no seu curriculum uma vasta gama de trabalhos
efetuados, e outros em curso a favor de empresas públicas, de referir: A PRODEL -
(Centrais Térmicas do Uíge, Luena, Tômbwa, Luau e Kuito), a EPAL EP e a ENDERNT.
Temos um contrato de fornecimento e manutenção preventiva com o Banco Sol
que compreende todas as Agências e Balcões de Luanda, Cacuaco e Viana;
responsabilizando também as Agências da referida Instituição bancária, assim como
outros clientes e empresas como AAA, BP que nos atestam a realização inquestionável
como garantiam, bem como a seriedade e a qualidade das obras nos trabalhos já
executados pela nossa empresa.</p>

        <p>Da vasta gama de atividades que satisfazem o objeto social desta empresa, temos como
objetivos fundamentais, a construção e montagem de grupos geradores, centrais
termoelétricas, estradas, caminhos-de-ferro, construção civil de vária índole e gestão de
empreendimentos nos mais variados sectores de atividades tais como água e despejos,
limpeza, arborização e jardinagem. Tudo isso consubstanciado numa holding que
aglutina várias forças que compõe as sinergias existentes entre elas.</p>
        

        <p>
        Para além do objetivo acima referido, também nos dedicamos na prestação de serviços
de reparação, manutenção preventiva e corretiva dos grupos geradores da marca que
comercializamos - a “GRUPEL”,MTU,CATERPILLAR assim como de outras marcas não
comercializadas por nós, prestação de serviços à terceiros, concorrendo eficazmente no
mercado primando sempre na qualidade de modo a incentivar os clientes a optarem
pelos nossos serviços, em conformidade com a demanda no mesmo. A nossa gestão é idónea e a administração é célere e eficiente; sendo portanto, motivo suficiente para
aposta dos clientes aos nossos dedicados serviços, tudo na nossa vontade férrea de
bem-fazer e na concretização dos seus objetivos preconizados.
        </p>

        <p>Em todos os sectores em que nós apostamos, na justeza do que somos, a nossa
habilidade não se esgota tão-somente nisto, mas também na melhor qualidade de
atendimento dos clientes, contando com os mais modernos recursos de comunicação,
etc., adequados com os mais modernos sistemas informáticos e de gestão, constituindo
assim uma equipa de profissionais qualificados, cujo trabalho é feito em perfeita
coordenação, com rigor e qualidade em todos os sectores adstritos.</p>

    <p>Operando no mercado angolano há cerca de 18 (Dezoito) anos, compreendemos de
uma maneira visionária as principais situações que afligem as populações e que
preocupam sobremaneira os governantes, tais como saneamento básico, redes de
águas e esgotos, energia e iluminação, estradas, pontes, etc., sendo porém, um dos
pressupostos a que nos dedicamos com mais profundidade ainda</p>
</div>
    </section>


    <section class="activities">
        <img style="margin:0px auto;" src="https://powerconlda.com/icons/organization.png">
    </section>

    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/1.png">
    </section>
    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/2.png">
    </section>
    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/3.png">
    </section>
    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/4.png">
    </section>
    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/5.png">
    </section>
    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/6.png">
    </section>
    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/7.png">
    </section>
    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/8.png">
    </section>
    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/9.png">
    </section>
    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/10.png">
    </section>
    <section class="activities">
        <img style="margin:0px auto; width: 530px;" src="https://powerconlda.com/icons/11.png">
    </section>


</main>
<footer class="footer_main">
    <div class="left">
        <p class="powercon">POWERCON</p>
        <p class="con1">Power Construction, Lda.</p>
        <br>
        <br>
    </div>
    <div class="right">
        
        <div class="right">
            <p><b>Escritório</b></p>
            <p>
                <b>Endereço</b>: Bairro Chinguar III Travessa B/22 S/N Benfica
                Luanda – Angola
            </p>
            <p> <b>Tel. Nº</b>  +244 933 527 548</p>
            <p><b>Email:</b>  geral@powerconlda.com</p>
    </div>
</footer>





@include("liveReload.script")

@endsection