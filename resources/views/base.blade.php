<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <meta name="description" content="Somos, uma empresa formada segundo as regras do direito angolano, registada no Ministério das Obras Públicas sob o Nº 0196/01 e portadora do cartão de contribuinte Nº 0.159.361/00-1, com data de 20 de Maio de 2002, cuja escritura de constituição se encontra publicada na III Série do Diário da República Nº 55, de 12 de Julho de 2002.">
    <meta name="keywords" content="Powercon - Construction, POWERCON, LDA, Lda ,Power Construction Limitada, Powerconlda, Benfica, construção civil, Fornecimento, gerador, luanda, reparação, Angola, angola, Luanda, materiais, construção civil, powercon, técnico, serviço, prestação, powerconlda.com, Bairro, Chinguar, III, Travessa, B/22, S/N Benfica,
    Luanda – Angola, NIF, 5403083090, 933 527 548, cris santos, filipinas, powercon lda">


    @yield('header')

</head>
<body>

    @yield('content')

</body>
</html>
