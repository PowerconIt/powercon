<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <meta name="description" content="Somos, uma empresa formada segundo as regras do direito angolano, registada no Ministério das Obras Públicas sob o Nº 0196/01 e portadora do cartão de contribuinte Nº 0.159.361/00-1, com data de 20 de Maio de 2002, cuja escritura de constituição se encontra publicada na III Série do Diário da República Nº 55, de 12 de Julho de 2002.">
    <meta name="keywords" content="Powercon - Construction, POWERCON, LDA, Lda ,Power Construction Limitada, Powerconlda, Benfica, construção civil, Fornecimento, gerador, luanda, reparação, Angola, angola, Luanda, materiais, construção civil, powercon, técnico, serviço, prestação, powerconlda.com, Bairro, Chinguar, III, Travessa, B/22, S/N Benfica,
    Luanda – Angola, NIF, 5403083090, 933 527 548, cris santos, filipinas, powercon lda">
    <link rel="stylesheet" href="/css/auth.css">
    <script src="/js/admin/auth.js"></script> 

    @yield('header')

</head>
<body >

    <div class="template mainApp">

            <div id="Auth" v-if="auth">
               
                <div class="form-conteiner" >
                    <div class="form" id="AUTHform">
                        <div class="logo">Powercon</div>
                        <div style="display: none" id="error-text"  
                            v-bind:class="{ force_show: error, animated: error, shake: error, fast: error }">
                            Registro não encontrado
                        </div>
                        <div class="input">
                            <form>
                                <div  class="text input-li">
                                    <label>Nome</label>  
                                    <div class="box">
                                        <span class="person"></span><input v-model="user" type="text" v-on:keyup.enter="login">
                                    </div>
                                </div>
                                <div class="password input-li">
                                    <label>Senha</label>
                                    <div class="box">
                                        <span class="lock"></span><input v-model="password" type="password" v-on:keyup.enter="login">
                                    </div>
                                </div>
                            </form>
    
                        </div>
                        
                        <div  @click="login" id="authButton" class="button_enter"> Entrar </div>
                    </div>
                </div>
              
            </div>
   

        <div id="admin" v-if="!auth">
            <header class="header_main">
            
                <div class="nav">
                    <div class="logo">
                        <a href="/"><img src="https://powerconlda.com/img/logo.png"></a>
                        
                    </div>
                    <nav>
                        <ul>
                            <li>Administration</li>
                            <li @click="logOut">
                                logout
                            </li>
                        </ul>
                    </nav>
                </div>
            
            
            
            </header>
            <main>
                <div class="count">
                    <div class="box">
                        <div class="title">Total visitors</div>
                        <div class="num">{{ $totalVisitors }}</div>
                    </div>
                    <div class="box">       
                        <div class="title">Unique visitors</div>
                        <div class="num">{{ $uniqueVisitors }}</div></div>
                    <div class="box">
                        <div class="title">Today</div>
                        <div class="num">{{ $today }}</div>
                    </div>


                </div>
                <table >
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>IP</th>
                            <th>Continent</th>
                            <th>Country</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Longitude</th>
                            <th>Latitude</th>
                          </tr>
                    </thead>
                    <tbody>
                        @foreach($visitors as $visitor)
                        <tr>
                            <td>{{ $visitor->id }}</td>
                            <td>{{ $visitor->ip }}</td>
                            <td>{{ $visitor->Continent }}</td>
                            <td>{{ $visitor->country }}</td>
                            <td>{{ $visitor->date }}</td>
                            <td>{{ $visitor->time }}</td>
                            <td>{{ $visitor->Longitude }}</td>
                            <td>{{ $visitor->Latitude }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>

            </main>
            <footer class="footer_main">
                <div class="left">
                    <p class="powercon">POWERCON</p>
                    <p class="con1">Power Construction, Lda.</p>
                    <br>
                    <br>
                </div>
                <div class="right">
                    
                    <div class="right">
                        <p><b>Escritório</b></p>
                        <p>
                            <b>Morada</b>: Bairro Chinguar III Travessa B/22 S/N Benfica
                            Luanda – Angola
                        </p>
                        <p> <b>Tel. Nº</b>  +244 933 527 548</p>
                        <p><b>Email:</b>  geral@powerconlda.com</p>
                </div>
            </footer>
        </div>
    </div>
</body>
</html>