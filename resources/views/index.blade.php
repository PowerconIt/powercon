@extends('base')

@section('title','Powercon')

@section('header')
    <style>
        body{margin:0;font-family:sans-serif;height:100%;width:100%;position:absolute;top:0;left:0;z-index:2;min-width:950px;font-family:Krub,sans-serif}body header.header_main{width:100%;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}body header.header_main .background{box-shadow:inset 0 -39px 97px #dedede;overflow:hidden}body header.header_main .background .conteiner{height:100%;width:100%;display:flex}body header.header_main .background .conteiner .gerador{height:100%;display:flex;width:40%;padding:67px 2% 4% 4%}body header.header_main .background .conteiner .gerador .img_conteiner{margin:0 auto;width:100%}body header.header_main .background .conteiner .gerador .img_conteiner img{width:100%}body header.header_main .background .conteiner .gerador .img_conteiner img:first-child{margin-bottom:-6px}body header.header_main .background .conteiner .info{font-size:15pt;color:#333;max-width:762px;padding:4% 4% 4% 2%;width:50%;text-align:justify;font-family:Krub,sans-serif;font-weight:500}body header.header_main .background .logo{margin-top:-150px;padding:0 4% 50px;font-family:Krub,sans-serif}body header.header_main .background .logo h1,body header.header_main .background .logo h2{color:#5e5e5e;margin:0}body header.header_main .background .logo h1{font-size:35px;font-weight:400}body header.header_main .background .logo h2{font-size:18px;font-weight:500}body header.header_main div.nav{display:flex;background:#fff;border-bottom:1px solid #d6d6d6;transition:1s;z-index:10000}body header.header_main div.nav .logo{width:80px}body header.header_main div.nav .logo img{width:100%}body header.header_main div.nav nav{z-index:100000}body header.header_main div.nav nav ul{margin-top:26px}body header.header_main div.nav nav ul li{display:inline;padding-left:30px;font-weight:600;font-size:13pt;cursor:pointer}body header.header_main div.nav nav ul li a{color:#000;text-decoration:none}body header.header_main .fixed{position:fixed;top:0;width:100%;box-shadow:0 10px 10px hsla(0,0%,79.6%,.47);border-bottom:1px solid #b9b9b9}body main.main_content{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;z-index:100;font-family:Krub,sans-serif}body main.main_content .activities,body main.main_content .contact,body main.main_content .galary,body main.main_content .info{border-bottom:1px solid #d6d6d6;display:flex;justify-content:space-between;padding-top:40px;padding-bottom:60px}body main.main_content .activities .left,body main.main_content .contact .left,body main.main_content .galary .left,body main.main_content .info .left{padding:0 0 0 5%}body main.main_content .activities .left .title,body main.main_content .contact .left .title,body main.main_content .galary .left .title,body main.main_content .info .left .title{max-width:300px;font-weight:600}body main.main_content .activities .right,body main.main_content .contact .right,body main.main_content .galary .right,body main.main_content .info .right{padding:0 5%;text-align:justify;width:60%;font-size:13pt;font-weight:400}body main.main_content .activities .right ul{padding:0;margin:0 0 0 21px;list-style:decimal;font-size:13pt;font-weight:500}body main.main_content .activities .right ul li{margin-bottom:5px}body main.main_content .activities .right ul ul{margin-bottom:15px;font-weight:400;list-style:disc}body main.main_content .activities .right ul ul ul{margin-bottom:5px;list-style:lower-alpha}body main.main_content .galary{background-color:#2d2d2d;box-shadow:inset 0 -52px 67px #1d1d1d;display:block;z-index:100;padding-bottom:40px}body main.main_content .galary p.showMore{color:#fff;text-align:center;margin-bottom:0}body main.main_content .galary p.arrow{margin:0;cursor:pointer;color:#fff;text-align:center}body main.main_content .galary p.title{text-align:center;margin:0 0 10px;font-weight:600;color:#fff}body main.main_content .galary .slide{display:flex;justify-content:center;width:100%}body main.main_content .galary .slide .container{width:95%;position:relative;color:#fff}body main.main_content .galary .slide .container #dots .active{background-color:#fff}body footer.footer_main{display:flex;padding-top:40px;padding-bottom:40px;justify-content:center}body footer.footer_main .left{display:flex;flex-wrap:wrap}body footer.footer_main .left p{margin:0;width:100%;padding-top:23px}body footer.footer_main .left .powercon{font-size:28pt;text-align:center}body footer.footer_main .left .con1{font-size:17pt!important;text-align:center}body footer.footer_main .left .con2,body footer.footer_main .left .con3,body footer.footer_main .left .con4,body footer.footer_main .left .con5,body footer.footer_main .left .con6,body footer.footer_main .left .con7{font-size:11pt;text-align:center;color:#49688d}
    </style>
    
    <link href="https://fonts.googleapis.com/css2?family=Krub:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;1,200;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">    
    <link rel="stylesheet" href="/css/glider.css">
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.7/css/all.css">        
    <script src="/js/index/index.js"></script>
    <link rel="stylesheet" href="/css/home.css">

@endsection

@section('content')

@include("include.contacte-nos")
    
<header class="header_main">
    
    <div class="background">
        <div class="conteiner">
            <div class="gerador">
                <div class="img_conteiner">
                    <img src="https://powerconlda.com/img/gerador.png">
                    <img src="https://powerconlda.com/img/gerador-shadow.png">
                </div>
                
            </div>
            <div class="info">
                <p>Somos, uma empresa formada segundo as regras do direito angolano, registada no Ministério das Obras Públicas sob o Nº 0196/01 e portadora do cartão de contribuinte Nº 0.159.361/00-1, com data de 20 de Maio de 2002, cuja escritura de constituição se encontra publicada na III Série do Diário da República Nº 55, de 12 de Julho de 2002. 
                </p>
                <p>Fornecimento de materiais para construção civil                            </p>
            </div>
        </div>       
        
        <div class="logo">
            <h1>POWERCON, LDA</h1>
            <h2>Power Construction Limitada</h2>
        </div>
                    
    </div>
    <div class="nav" >
        <div class="logo">
            <img src="https://powerconlda.com/img/logo.png">
        </div>
        <nav>
            <ul>
                <li><a href="/">Inicío</a></li>
                <li> <a href="/#actividades_hash">Áreas e actividades do nosso objecto social</a>  </li>
                <li><a href="/galeria">Galeria</a></li>
                <li class="openContact">Contacte-nos</li>
                <li><a href="https://www.facebook.com/Powercon-Construction-Limitada-111545377422553/">Facebook</a></li>
                <li><a href="/perfil">Perfil</a></li>
            </ul>
        </nav>
    </div>



</header>
<main class="main_content">
    <section class="info">
        <div class="left" id="actividades_hash">
            <div id="cover">
                <img src="https://powerconlda.com/img/intro.jpg">
            </div>
            
        </div>
        <div class="right">Embora compreendamos, quanto maçadores são estes aspetos burocráticos, em nosso entender, eles são essenciais pois, revelam, à partida, uma transparência de que não queremos privar os nossos clientes. Além do que, as atividades a que nos dedicamos assim o impõem.
            Assim, temos o enorme prazer de facultar a V. Exas., informação sobre quais as áreas ou atividades que preenchem o nosso objetos social
        </div>
    </section>
    <section class="activities">
        <div class="left">
            <p class="title">ÁREAS E ACTIVIDADES <br>DO NOSSO OBJECTO SOCIAL</p>
        </div>
        <div class="right">
            <ul>
                <li>Execução de obras de construção civil</li>
                <ul>
                    <li>Saneamento Básico</li>
                    <ul>
                        <li>Redes de águas e esgotos</li>
                    </ul>
                    <li>Execução de obras de construção civil</li>
                </ul>
                <li>Fornecimento de materiais para construção civil</li>
                <ul>
                    <li>oferecemos todos os tipos de materiais e equipamentos, de acordo com as exigências dos nossos clientes</li>
                    <ul>
                        <li>Quadro inversor automatico, APP Doméstico, Comercial e Industrial, Quadro de sincronismo e controle</li>

                    </ul>
                </ul>
                <li>Outros sectores de atividades</li>
                <ul>
                    <li>Energia Eléctrica grupo gerador</li>
                    <ul>
                        <li>Alta, média e baixa tensão</li>
                    </ul>
                    <li>Projecção, fornecimento e instalação de todos os tipos de equipamentos nos seguintes sectores</li>
                    <ul>
                        <li>Estações de bombagem de águas residuais e/ou potáveis, 
                            Condutas e válvulas
                        </li>
                        <li>
                            Estações de bombagem de água para irrigação e        sistemas de rega
                        </li>
                    </ul>
                </ul>

            </ul>
        </div>
    </section>
    <section class="galary">

        <p class="title">Slide</p>

        <div class="slide">

            <section class="container">
                <button class="glider-prev">
                    <i class="fas fa-chevron-circle-left"></i>
                </button>
    
                <div class="glider">
                    <div class="card">
                        <img src="https://powerconlda.com/img/slide/DSC00657.JPG?fsdfsdf" alt="">
                    </div>
                    <div class="card">
                        <img src="https://powerconlda.com/img/slide/DSC00658.JPG?fsdfsdf" alt="">
                    </div>
                    <div class="card">
                        <img src="https://powerconlda.com/img/slide/20200130_113814.jpg?fsdfsdf" alt="">
                    </div>
                    <div class="card">
                        <img src="https://powerconlda.com/img/slide/Picture-129.jpg?fsdfsdf" alt="">
                    </div>
                    <div class="card">
                        <img src="https://powerconlda.com/img/slide/gerador.jpg?fsdfsdf" alt="">
                    </div>
                    <div class="card">
                        <img src="https://powerconlda.com/img/slide/DSC_0088.jpg?fsdfsdf" alt="">
                    </div>
                    <div class="card">
                        <img src="https://powerconlda.com/img/slide/IMG00232.jpg?fsdfsdf" alt="">
                    </div>

                </div>
                <div id="dots"></div>
    
                <button class="glider-next">
                    <i class="fas fa-chevron-circle-right"></i>
                </butto>
    
            </section>
            
    
        </div>

        <!--<p class="showMore">Mostra mais</p>  -->
        <!-- <p class="arrow"><i class="fas fa-chevron-down"></i></p>-->


    </section>
</main>
<footer class="footer_main">
    <div class="left">
        <p class="powercon">POWERCON</p>
        <p class="con1">Power Construction, Lda.</p>
        <br>
        <br>
    </div>
    <div class="right">
        
        <div class="right">
            <p><b>Escritório</b></p>
            <p>
                <b>Endereço</b>: Bairro Chinguar III Travessa B/22 S/N Benfica
                Luanda – Angola
            </p>
            <p> <b>Tel. Nº</b>  +244 933 527 548</p>
            <p><b>Email:</b>  geral@powerconlda.com</p>
    </div>
</footer>





@include("liveReload.script")

@endsection