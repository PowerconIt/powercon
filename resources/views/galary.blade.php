@extends('base')

@section('title','Powercon - Galeria')

@section('header')

<style>
    body{margin:0;font-family:sans-serif;height:100%;width:100%;position:absolute;top:0;left:0;z-index:2;min-width:950px;font-family:Krub,sans-serif;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}body header.header_main{width:100%;box-shadow:inset 0 -39px 97px #dedede}body header.header_main .conteiner{width:100%;display:flex}body header.header_main .conteiner .info{font-size:15pt;color:#333;max-width:762px;padding:4% 4% 4% 2%;width:50%;text-align:justify;font-family:Krub,sans-serif;font-weight:500}body div.nav{display:flex;background:#fff;border-bottom:1px solid #d6d6d6;box-shadow:0 10px 10px hsla(0,0%,79.6%,.47);z-index:10000;top:0;padding:0 4%}body div.nav .logo{width:80px}body div.nav .logo img{width:100%}body div.nav nav ul{margin-top:26px}body div.nav nav ul li{display:inline;padding-left:30px;font-weight:600;font-size:13pt;cursor:pointer}body div.nav nav ul li a{color:#000;text-decoration:none}body h2{text-align:center;padding-bottom:12px}body .main_content h2{margin-top:50px}body .main_content .galary{background-color:#2d2d2d;box-shadow:inset 0 -52px 67px #1d1d1d;display:block;padding-bottom:15px;z-index:100;padding-top:30px}body .main_content .galary p.showMore{color:#fff;text-align:center;margin-bottom:0}body .main_content .galary p.arrow{margin:0;cursor:pointer;color:#fff;text-align:center}body .main_content .galary p.title{text-align:center;margin:0 0 10px;font-weight:600;color:#fff;font-size:15pt}body .main_content .galary .slide{display:flex;justify-content:center;width:100%}body .main_content .galary .slide .container{width:95%;position:relative;color:#fff}body .main_content .galary .slide .container .glider{display:flex;flex-wrap:wrap;justify-content:space-evenly}body .main_content .galary .slide .container .glider .card{width:49%}body .main_content .galary .slide .container .glider .card img{width:100%}body footer.footer_main{display:flex;padding-top:40px;padding-bottom:40px;justify-content:center}body footer.footer_main .left{display:flex;flex-wrap:wrap}body footer.footer_main .left p{margin:0;width:100%;padding-top:23px}body footer.footer_main .left .powercon{font-size:28pt;text-align:center}body footer.footer_main .left .con1{font-size:17pt!important;text-align:center}body footer.footer_main .left .con2,body footer.footer_main .left .con3,body footer.footer_main .left .con4,body footer.footer_main .left .con5,body footer.footer_main .left .con6,body footer.footer_main .left .con7{font-size:11pt;text-align:center;color:#49688d}
</style>

<link rel="stylesheet" href="/css/galary.css?fsdfsdf">
<link href="https://fonts.googleapis.com/css2?family=Krub:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;1,200;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">       
<link rel="stylesheet" href="//use.fontawesome.com/releases/v5.0.7/css/all.css">        
<script src="/js/galary/galaryShowMore.js?fsdfsdf"></script> 


@endsection


@section('content')

@include("include.contacte-nos")

<header class="header_main">
            
    <div class="nav">
        <div class="logo">
            <img src="https://powerconlda.com/img/logo.png">
        </div>
        <nav>
            <ul>
                <li><a href="/">Inicío</a></li>
                <li><a href="/#actividades_hash">Áreas e actividades do nosso objecto social</a> </li>
                <li><a href="/galeria">Galeria</a></li>
                <li class="openContact">Contacte-nos</li>
                <li><a href="https://www.facebook.com/Powercon-Construction-Limitada-111545377422553/">Facebook</a></li>
                <li><a href="/perfil">Perfil</a></li>
            </ul>
        </nav>
    </div>



</header>

<main class="main_content" >
    
   <div id="main_content"></div>

</main>

<footer class="footer_main">
    <div class="left">
        <p class="powercon">POWERCON</p>
        <p class="con1">Power Construction, Lda.</p>
        <br>
        <br>
    </div>
    <div class="right">
        
        <div class="right">
            <p><b>Escritório</b></p>
            <p>
                <b>Morada</b>: Bairro Chinguar III Travessa B/22 S/N Benfica
                Luanda – Angola
            </p>
            <p> <b>Tel. Nº</b>  +244 933 527 548</p>
            <p><b>Email:</b>  geral@powerconlda.com</p>
    </div>
</footer>


@include("liveReload.script");

@endsection